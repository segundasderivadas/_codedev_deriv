import sys
import os
import numpy as np
import matplotlib.pylab as plt
funcPath = os.path.dirname(__file__)
mydir = r'C:\Users\eemiliosl\Documents\phd\_codedev'
sys.path.append(os.path.abspath(os.path.join(mydir, 'myfunctions_git')))
import deriv_functions as df


def main():
    # Define directories
    model_folder = os.path.abspath(os.path.join('.', '..', 'results_diag_plots', 'synthetic_data'))
    # model_folder = r'C:\Users\eemiliosl\Dropbox\mexico_diag_plot\results_diag_plots\synthetic_data'
    obswell_file = 'confined_simple_s_3_layers_invertedo.observation_well_flow.a.dat'
    pumpwell_file = 'confined_simple_s_3_layers_invertedo.observation_well_flow.pumping.dat'
    pw = np.loadtxt(os.path.join(model_folder, pumpwell_file), skiprows=2, usecols=(0, 1))
    ow = np.loadtxt(os.path.join(model_folder, obswell_file), skiprows=2, usecols=(0, 1))
    ini_head = 100.

    # Calculate with Bourdet:
    dtdlnx = df.bourdetl(ow[:, 0], ini_head - ow[:, 1], l=0.0, logx=True)
    # dtdlnx2 = df.bourdetl(pw[:, 0], ini_head - pw[:, 1], l=0.0, logx=True)
    # Calculate direct solution:
    dtdlnx_direct = df.dsdlnt_numerically(ow[:, 0], ini_head - ow[:, 1], mode='direct_num')
    # Calculate using definition:
    dtdlnx_def = df.dsdlnt_numerically(ow[:, 0], ini_head - ow[:, 1], mode='def')

    # Plot all together:
    plt.loglog(ow[:, 0], ini_head - ow[:, 1], 'b-x', label='Drawdown vs time')
    plt.loglog(ow[:, 0], dtdlnx, 'g-o', label='Bourdet')
    plt.loglog(ow[1:, 0], dtdlnx_def, 'r-x', label='Derivative-by def')
    plt.loglog(ow[1:, 0], dtdlnx_direct, 'k-x', label='Derivative-direct')
    plt.grid(True)
    plt.legend(loc='best')
    plt.xlabel('Log time [seconds]')
    plt.ylabel('Log Drawdown [meters]')

    # Standard plots for drawdown versus time
    f, axarr = plt.subplots(3, 1)
    axarr[0].plot(pw[:, 0], ini_head - pw[:, 1], 'bo-', label='pumping well')
    axarr[0].plot(ow[:, 0], ini_head - ow[:, 1], 'ro-', label='observation well')
    axarr[0].grid(True)
    axarr[0].legend(loc='best')
    axarr[0].set_title('dwdn vs Time ')
    axarr[1].loglog(pw[:, 0], ini_head - pw[:, 1], 'bo-', label='pumping well')
    axarr[1].loglog(ow[:, 0], ini_head - ow[:, 1], 'ro-', label='observation well')
    axarr[1].grid(True)
    axarr[1].legend(loc='best')
    axarr[1].set_title('Log-log dwdn vs Time ')
    axarr[2].semilogx(pw[:, 0], ini_head - pw[:, 1], 'bo-', label='pumping well')
    axarr[2].semilogx(ow[:, 0], ini_head - ow[:, 1], 'ro-', label='observation well')
    axarr[2].grid(True)
    axarr[2].legend(loc='best')
    axarr[2].set_title('Semi-log dwdn vs Time ')
    plt.show()
    # plt.close()

    df.deriv_plot(ow[:, 0], ini_head - ow[:, 1], dtdlnx, axestype='loglog')
    np.savetxt(os.path.join(os.path.curdir, 'output.txt'), np.transpose((ow[:, 0], dtdlnx)))

    # To plot one model vs another
    # plt.loglog(ow[:, 0], ini_head - ow[:, 1], 'b-x', label='Drawdown vs time 5 K layers')
    # plt.loglog(ow[:, 0], dtdlnx, 'b-o', label='Bourdet 5 K layers')
    # plt.loglog(pw[:, 0], ini_head - pw[:, 1], 'r-o', label='Drawdown vs time 3 K layers')
    # plt.loglog(pw[:, 0], dtdlnx2, 'r-x', label='Bourdet 3 K layers')
    # plt.grid(True)
    # plt.legend(loc='best')
    # plt.xlabel('Log time [seconds]')
    # plt.ylabel('Log Drawdown [meters]')

    # dtdlnx_o = df.bourdet(st, dp, logx=False)
    # Plot stuff:
    # deriv_plot(xdata, ydata, dtdx, mylabel1='', mylabel2='', xlab='', ylab='', mytitle='', axestype='arithmetic'):
    # df.deriv_plot(st, dp, dtdlnx, axestype='arithmetic')
    # df.deriv_plot(st, dp, dtdlnx_o, axestype='arithmetic')
    # dp = np.loadtxt(os.path.join(r'C:\Users\eemiliosl\Dropbox\mexico_diag_plot\_codedev_deriv\matlab', 'dp.txt'))
    # st = np.loadtxt(os.path.join(r'C:\Users\eemiliosl\Dropbox\mexico_diag_plot\_codedev_deriv\matlab', 'st.txt'))
    print('Process finished')
if __name__ == '__main__':
    main()
