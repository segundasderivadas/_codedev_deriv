""" Generate a random field to construct the reference scenario for the inversion.
Includes the extraction of "field" observtions """

import sys
import os
import scipy.sparse as ssp
import numpy as np
import matplotlib.pylab as plt
funcPath = os.path.dirname(__file__)
mydir = r'C:\Users\eemiliosl\Documents\phd\_codedev'
sys.path.append(os.path.abspath(os.path.join(mydir, 'myfunctions_git')))
import dir_manip as dm
import grid_manip as gm
import hgs_grok as hg
import gen_fields as gf


def main():
    # Define directories:

    mainPath = r'C:\Users\eemiliosl\Dropbox\mexico_diag_plot\_models'  # os.path.dirname(__file__)
    curKFile = 'kkkGaus.dat'
    mymode = 'fl_'  # 'tr_'
    mymodel = 'theis_copy'  # '_tr_20062015_synthetic_b3top_fluor'
    Flowdir = 'confined_homo'  # Used only if transport is to be run
    modelDataFolder = 'ModelData_confined_hetero'
    std_dev = 2.5e-6  # As an example assumed a std error measurement of 1.0 mm on each measurement 1.5e-3, tr = 2.5e-6
    runmodel = False
    genfield = False

    # -- #
    curDestDir = os.path.join(mainPath, mymodel)

    # After GROK file for Flow has been created, generate parameter field:
    # Make corresponding model run first:
    # Read parameter field setup:
    if (not os.path.isdir(os.path.join(curDestDir, 'kkkGaus.dat'))) and (genfield is True):
        grid_filedir = os.path.join(curDestDir, 'meshtecplot.dat')
        myParameterDir = os.path.join(mainPath, modelDataFolder, '_RandFieldsParameters.dat')

        xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = gf.readParam(
            myParameterDir)

        nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)
        biggrid_elem_ids = np.arange(1, elements + 1, 1)
        smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)

        kkkvalues = gf.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel,
                                        Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot=True,
                                        mydir=curDestDir)
        kkk = gm.expshr_param(kkkvalues, biggrid_elem_ids, smallgrid_elem, what='expand')
        np.savetxt(os.path.join(curDestDir, curKFile), np.transpose((np.arange(1, len(kkk) + 1, 1), kkk, kkk, kkk)),
                   fmt='%d %.6e %.6e %.6e')
        kkkvalues = np.loadtxt(os.path.join(curDestDir, 'kkkGaus.dat'), usecols=(1,))
        if mymode is 'tr_':
            kkkvalues = kkkvalues[smallgrid_elem]
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(kkkvalues) + 1, 1), kkkvalues, kkkvalues, kkkvalues)),
                       fmt='%d %.6e %.6e %.6e')

            headsFile = dm.getdirs(os.path.join(curDestDir, '..', Flowdir), mystr='head_pm', fullpath=True,
                                   onlylast=True)
            myheadsFlow, mytime = hg.readHGSbin(headsFile[0], nnodes)
            myheadsTransport = myheadsFlow[smallgrid_nodes]
            np.savetxt(os.path.join(curDestDir, "finalheadsFlow.dat"), np.transpose(myheadsTransport))

    if runmodel is True:
        hg.fwdHGS(curDestDir, 'reference', mytype=mymode, hsplot=True)

    # Get the "field" observations:
    # -----------------------------

    # Extract modeled observations:
    myouttimes = np.loadtxt(os.path.join(mainPath, modelDataFolder, '_outputtimes%s.dat' % mymode))
    selObs_All = hg.getmodout(curDestDir, mode=mymode, outputtime=myouttimes, initial_head=30.0)

    # Add gaussian measurement noise:
    num_modmeas = selObs_All.shape[0]
    meanNoise = np.zeros((num_modmeas,))  # Mean value of white noise equal to zero (num_modmeas x 1)
    R = ssp.dia_matrix((np.multiply(np.eye(num_modmeas, num_modmeas), std_dev ** 2)))  # sparse Matrix (nmeas x nmeas)
    E = np.empty((num_modmeas, selObs_All.shape[1]))
    for ii in range(0, selObs_All.shape[1], 1):
        # E[:,ii] = np.expand_dims(np.random.multivariate_normal(mean,R.toarray()),axis = 1)
        E[:, ii] = np.random.multivariate_normal(meanNoise, R.toarray())

    noisy_data = selObs_All + E

    # For concentrations
    if mymode is 'tr_':
        zero_id = np.where(noisy_data < 0)
        noisy_data[zero_id[0], zero_id[1]] = 0

    # Save artificial field measurements:
    np.savetxt(os.path.join(mainPath, modelDataFolder, 'Field%s.dat' % mymodel), noisy_data, fmt='%10.9e')
    print('Reference model done and field observations stored')


if __name__ == '__main__':
    main()
