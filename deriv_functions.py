import numpy as np
import matplotlib.pylab as plt


def bourdetl(xdata, ydata, l=0, logx=True):
    """
        Calculates the log derivative according to Bourdet.
        Paper: {Bourdet, 1989}
    Arguments:
        xdata:  np.array, vector with x (eg time) data
        ydata:  np.array, vector with y (eg pressure change, drawdown) data
        l:      float, l factor according to Bourdet (max = 0.5). Default = 0
        logx:   bool, calculate derivatives using ln(t). If tru, time should be given NOT in log scale.
    Returns:
        dtdx:   np.array, vector of size = len(xdata), with derivatives
    """
    # Initialize derivative vector:
    dtdx = np.zeros((len(xdata),))

    # Calculate the log - derivative (numerically) at the extremes:
    dpo = np.abs(ydata[1] - ydata[0])
    dpf = np.abs(ydata[-1] - ydata[-2])
    if logx is True:
        dto = np.abs(np.log(xdata[1]) - np.log(xdata[0]))
        dtf = np.abs(np.log(xdata[-1]) - np.log(xdata[-2]))
    else:
        dto = np.abs(xdata[1] - xdata[0])
        dtf = np.abs(xdata[-1] - xdata[-2])

    dtdx[0] = dpo / dto
    dtdx[-1] = dpf / dtf

    for xx in range(1, len(dtdx)-1, 1):
        # index i1
        for jj in reversed(range(0, xx, 1)):
            # Left extreme reached:
            if jj == 0:
                i1 = 0
                break
            else:
                # Compare to L
                if logx is True:
                    mydif = np.log(xdata[xx]) - np.log(xdata[jj])
                else:
                    mydif = xdata[xx] - xdata[jj]
                if mydif >= l:
                    i1 = jj
                    break
        # index 12
        for jj in range(xx+1, len(dtdx), 1):
            # Right extreme reached
            if jj == len(dtdx):
                i2 = len(dtdx)
                break
            else:
                # Compare to L
                if logx is True:
                    mydif = np.log(xdata[jj]) - np.log(xdata[xx])
                else:
                    mydif = xdata[jj] - xdata[xx]
                if mydif >= l:
                    i2 = jj
                    break
        # Calculate all differences first:
        dydata1 = np.abs(ydata[xx] - ydata[i1])
        dydata2 = np.abs(ydata[i2] - ydata[xx])

        if logx is True:
            dxdata1 = np.abs(np.log(xdata[xx]) - np.log(xdata[i1]))
            dxdata2 = np.abs(np.log(xdata[i2]) - np.log(xdata[xx]))
        else:
            dxdata1 = np.abs(xdata[xx] - xdata[i1])
            dxdata2 = np.abs(xdata[i2] - xdata[xx])

        # Calculate derivatives:
        dtdx[xx] = (((dydata1 / dxdata1) * dxdata2) + ((dydata2 / dxdata2) * dxdata1)) / (dxdata1 + dxdata2)

    return dtdx


def bourdet(xdata, ydata, logx=True):
    """
    Calculates the log derivative according to Bourdet. With L fixed to 0
        Paper: {Bourdet, 1989}
    Arguments:
        xdata:  np.array, vector with x (eg time) data
        ydata:  np.array, vector with y (eg pressure change, drawdown) data
        logx:   bool, calculate derivatives using ln(t). If tru, time should be given NOT in log scale.
    Returns:
        dtdx:   np.array, vector of size = len(xdata), with derivatives
    """
    npo = 1
    dtdx = np.zeros((len(ydata),))

    if logx is True:
        dtdx[0] = np.abs(ydata[1] - ydata[0]) / np.abs(np.log(xdata[1]) - np.log(xdata[0]))
        dtdx[-1] = np.abs(ydata[-1] - ydata[-2]) / np.abs(np.log(xdata[-1]) - np.log(xdata[-2]))
    else:
        dtdx[0] = np.abs(ydata[1] - ydata[0]) / np.abs(xdata[1] - xdata[0])
        dtdx[-1] = np.abs(ydata[-1] - ydata[-2]) / np.abs(xdata[-1] - xdata[-2])

    for jj in range(npo, len(ydata) - npo):
        cur_id = jj
        dp1 = np.abs(ydata[cur_id] - ydata[cur_id - npo])
        dp2 = np.abs(ydata[cur_id] - ydata[cur_id + npo])
        if logx is True:
            dt1 = np.abs(np.log(xdata[cur_id]) - np.log(xdata[cur_id - jj]))
            dt2 = np.abs(np.log(xdata[cur_id]) - np.log(xdata[cur_id + jj]))
        else:
            dt1 = np.abs(xdata[cur_id] - xdata[cur_id - npo])
            dt2 = np.abs(xdata[cur_id] - xdata[cur_id + npo])
        # while dt1 <= L:
        #     npo =+ 1
        #     dt1 = np.abs(np.log(mytime[cur_id]) - np.log(mytime[cur_id - npo]))
        # while dt2 < = L:
        #     npo = + 1
        #     dt2 = np.abs(np.log(mytime[cur_id]) - np.log(mytime[cur_id + npo]))
        dtdx[jj] = (((dp1 / dt1) * dt2) + ((dp2 / dt2) * dt1)) / (dt1 + dt2)

    return dtdx


def dsdlnt_numerically(xdata, ydata, mode='def'):
    """
    Calculate ds/dlnt numerically, following either
    strict definition: ds/dlnt = t * ds/dt
    direct numerical differentiation:  ds/dlnt = s1 -ds2 / ( ln(t1) - ln(t2)
        Arguments:
    xdata:      np.array, 1D array with x-Data
    ydata:      np.array, 1D array with y-Data
    mode:       mode to estimate derivatives: 'def' or 'direct_num'
                'def': dsdlnt[ii] = xdata[ii] * ((ydata[ii + 1] - ydata[ii]) / (xdata[ii + 1] - xdata[ii]))
                'direct_num': dsdlnt[ii] = (ydata[ii + 1] - ydata[ii]) / (np.log(xdata[ii + 1]) - np.log(xdata[ii]))
        Returns:
    dsdlnt:     1D np array of ds/dlnt. Length equals initial length - 1
    """
    dsdlnt = np.empty((len(ydata) - 1, 1))

    for ii in range(0, len(dsdlnt)):
        # By definition
        if mode is 'def':
            dsdlnt[ii] = xdata[ii] * ((ydata[ii + 1] - ydata[ii]) / (xdata[ii + 1] - xdata[ii]))
        # Numerically
        elif mode is 'direct_num':
            dsdlnt[ii] = (ydata[ii + 1] - ydata[ii]) / (np.log(xdata[ii + 1]) - np.log(xdata[ii]))
        else:
            print('method to estimate derivatives not supported')
            break

    return dsdlnt


def deriv_plot(xdata, ydata,  dtdx, mylabel1='', mylabel2='', xlab='', ylab='', mytitle='', axestype='arithmetic'):
    """
        Arguments:
    xdata:          np.array, 1D array with x-Data
    ydata:          np.array, 1D array with y-Data
    dtdx:           np.array with derivatives
    mylabel1:       str, label for ydata series
    mylabel2:       str, label for dtdx series
    xlab:           str, xaxis label
    ylab:           str, yaxis label
    mytitle:        str, plot title
    axestype:       str, scale for the axis. 'arithmetic', 'loglog', 'semilogx', 'semilogy'
                    default is arithmetic
        Return:
    plot in a figure window
    """
    f, axarr = plt.subplots(1, 1)
    if axestype is 'arithmetic':
        axarr.plot(xdata, ydata, 'k-', label=mylabel1)
        axarr.plot(xdata, dtdx, 'b--', label=mylabel2)
    elif axestype is 'loglog':
        axarr.loglog(xdata, ydata, 'k-', label=mylabel1)
        axarr.loglog(xdata, dtdx, 'b--', label=mylabel2)
    elif axestype is 'semilogx':
        axarr.semilogx(xdata, ydata, 'k-', label=mylabel1)
        axarr.semilogx(xdata, dtdx, 'b--', label=mylabel2)
    elif axestype is 'semilogy':
        axarr.semilogy(xdata, ydata, 'k-', label=mylabel1)
        axarr.semilogy(xdata, dtdx, 'b--', label=mylabel2)

    plt.grid(True)
    plt.legend(loc='best')
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    axarr.set_title(mytitle)


# def main():
#     # Define directories
#
#     model_folder = r'C:\Users\eemiliosl\Dropbox\mexico_diag_plot\_models\confined_homo'
#     pw = np.loadtxt(os.path.join(model_folder, 'confined_simpleo.observation_well_flow.pumping.dat'), skiprows=2, usecols=(0, 1))
#     ow = np.loadtxt(os.path.join(model_folder, 'confined_simpleo.observation_well_flow.a.dat'), skiprows=2, usecols=(0, 1))
#     ini_head = 30.
#
#     f, axarr = plt.subplots(3, 1)
#     axarr[0].plot(pw[:, 0], ini_head-pw[:, 1], 'bo-', label='pumping well')
#     axarr[0].plot(ow[:, 0], ini_head-ow[:, 1], 'ro-', label='observation well')
#     axarr[0].grid(True)
#     axarr[0].legend(loc='best')
#     axarr[0].set_title('dwdn vs Time ')
#
#     axarr[1].loglog(pw[:, 0], ini_head-pw[:, 1], 'bo-', label='pumping well')
#     axarr[1].loglog(ow[:, 0], ini_head-ow[:, 1], 'ro-', label='observation well')
#     axarr[1].grid(True)
#     axarr[0].legend(loc='best')
#     axarr[1].set_title('Log-log dwdn vs Time ')
#
#     axarr[2].semilogx(pw[:, 0], ini_head-pw[:, 1], 'bo-', label='pumping well')
#     axarr[2].semilogx(ow[:, 0], ini_head-ow[:, 1], 'ro-', label='observation well')
#     axarr[2].grid(True)
#     axarr[0].legend(loc='best')
#     axarr[2].set_title('Semi-log dwdn vs Time ')
#     plt.close()
#     # mytime, testf = np.loadtxt(
#     #     r'C:\Users\eemiliosl\Dropbox\mexico_diag_plot\_models\synthetic_data\synthetic_theis_copy_modif.txt',
#     #     usecols=(0, -1), unpack=True)
#     mytime, testf = ow[:, 0], ini_head - ow[:, 1]
#     dsdlnt = np.zeros((len(testf)-1, 1))
#     dsdlnt1 = np.zeros((len(testf) - 1, 1))
#
#
#     for xx in range(1,2):
#         npo = xx
#         dtdx = np.zeros((len(testf) - npo, 1))
#         for jj in range(npo, len(testf)-npo):
#             # Bourdet
#             cur_id = jj
#             dp1 = np.abs(testf[cur_id]-testf[cur_id-npo])
#             dp2 = np.abs(testf[cur_id]-testf[cur_id+npo])
#             dt1 = np.abs(np.log(mytime[cur_id])-np.log(mytime[cur_id-npo]))
#             dt2 = np.abs(np.log(mytime[cur_id]) - np.log(mytime[cur_id + npo]))
#             # while dt1 <= L:
#             #     npo =+ 1
#             #     dt1 = np.abs(np.log(mytime[cur_id]) - np.log(mytime[cur_id - npo]))
#             # while dt2 < = L:
#             #     npo = + 1
#             #     dt2 = np.abs(np.log(mytime[cur_id]) - np.log(mytime[cur_id + npo]))
#             dtdx[jj] = (((dp1/dt1)*dt2) + ((dp2/dt2)*dt1)) / (dt1+dt2)
#
#
#     print('pause')
# if __name__ == '__main__':
#     main()
