% ------------------------------------ 
% der_Bourdet.m
% ------------------------------------
% Josué Tago Pacheco
% ------------------------------------
% Descripción:
% Se calcula la derivada de Bourdet
% con los datos del artículo original
% ------------------------------------

close all
clear all

% Algunos mensajes
disp('Se calcula la derivada de Bourdet con los datos del artículo original');

% Parámetro de suavizamiento
L = input('Asigna el valor de L : ');

% --- Lectura de los datos ---
% Superposition time (x)
x = load('st.txt');
% Delta de presion (y)
y = load('dp.txt');

% Se obtiene el número de datos
ndata = length(x);

% Se inicializa la derivada que se calculara con Bourdet
dB(1:ndata)=0.;

% Se calculan las derivadas en los extremos
% x_min
dB(1) = (y(2)-y(1))/(x(2)-x(1));
% x_max
dB(ndata) = (y(ndata)-y(ndata-1))/(x(ndata)-x(ndata-1));

% Se calculan el resto de las derivadas
for i=2:(ndata-1)

   % --- Se calcula x1 (índice i1) ---
   for j=(i-1):-1:1
      % Se alcanzo el extremo izquierdo
      if (j == 1) 
         i1 = 1;
         break;
      else
         % Se hace la comparación con respecto a L
         if ( (x(i)-x(j)) >= L ) 
            i1 = j;
            break;
         end 
      end
   end

   % --- Se calcula x2 (índice i2) ---
   for j=(i+1):ndata
      % Se alcanzo el extremo derecho
      if (j == ndata) 
         i2 = ndata;
         break;
      else
         % Se hace la comparación con respecto a L
         if ( (x(j)-x(i)) >= L ) 
            i2 = j;
            break;
         end
      end
   end  

   % Se calculan todas las diferencias
   % y
   dy1 = y(i)-y(i1);
   dy2 = y(i2)-y(i);
   % x
   dx1 = x(i)-x(i1);
   dx2 = x(i2)-x(i);
   % Se hace el cálculo de la derivada
   dB(i) = ((dy1/dx1)*dx2 + (dy2/dx2)*dx1) / (dx1+dx2);

end

% Se graba la salida
save -ascii dB.txt dB

% Se grafica la función y su derivada calculada con Bourdet
plot(x,y);
hold on;
plot(x,dB,'r');
legend('Funcion','Derivada de Bourdet');

% Se cargan las derivadas de Bourdet del artículo
%dBl0_art = load('ppl0.txt'); % L = 0.0
%dBl0p1_art = load('ppl0p1.txt'); % L = 0.1

% Se grafican las derivadas de datos y las calculadas
% figure
% plot(x,dBl0_art);
% hold on;
% plot(x,dBl0p1_art,'g');
% plot(x,dB,'r');
% legend('Bourdet L = 0.0 (Art)','Bourdet L = 0.1 (Art)','Bourdet (Calculado)');







