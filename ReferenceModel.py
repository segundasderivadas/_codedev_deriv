""" Generate a random field for synthetic models
Includes the extraction of "field" observations """

import sys
import os
import scipy.sparse as ssp
import numpy as np
import matplotlib.pylab as plt
funcPath = os.path.dirname(__file__)
mydir = r'C:\Users\eemiliosl\Documents\phd\_codedev'
sys.path.append(os.path.abspath(os.path.join(mydir, 'myfunctions_git')))
import dir_manip as dm
import grid_manip as gm
import hgs_grok as hg
import gen_fields as gf

def main():

    #%% Define directories:
    mainPath = os.path.dirname(__file__)
    curKFile = 'kkkGaus.dat'
    curDestDirFlow = os.path.join(mainPath,'Flow')
    curDestDirTrans = os.path.join(mainPath,'Transport')
    grid_filedir = os.path.join(curDestDirFlow, 'meshtecplot.dat')
    #%% After GROK file for Flow has been created, generate paramweter field:
    myDir = os.path.join(mainPath, '..', 'Model_data', '_RandFieldsParameters.dat')#r'C:\PhD_Emilio\Emilio_PhD\_CodeDev\Kalman_filter\Data_forModel\_RandFieldsParameters.dat')
##    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = gf.readParam(myDir)
##
##    nnodes, elements = gm.readmesh(grid_filedir, fulloutput = False)
##    biggrid_elem_ids = np.arange(1,elements+1,1)
##    smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)
##
##    #%% Make Flow model run first:
##    # Read parameter field setup:
##
##    kkkvalues = gf.genFields_FFT_3d(xlim[1]-xlim[0], ylim[1]-ylim[0], zlim[1]-zlim[0], nxel, nyel, nzel, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot = True, mydir = curDestDirFlow)
##    kkk = gm.expshr_param(kkkvalues, biggrid_elem_ids, smallgrid_elem, what = 'expand')
##    np.savetxt(os.path.join(curDestDirFlow, curKFile), np.transpose((np.arange(1,len(kkk)+1,1), kkk, kkk, kkk)), fmt='%d %.6e %.6e %.6e')
##    hg.fwdHGS(curDestDirFlow, 'reference', mytype = 'Flow' , hsplot = True)
##
##    #%% After running the flow model, prepare for transport:
##
##    # Extract final heads and kkk for the transient model (small grid) from the flow run (big grid):
##        # Get the final-steady heads from flow simulation
##    headsFile = dm.getdirs(curDestDirFlow, mystr = 'head_pm', fullpath = True, onlylast = True) #!!!!! Is not Flow directory? No because a copy is done when preparing Grok for transport
##
##    myheadsFlow, mytime = hg.readHGSbin(headsFile[0], nnodes)
##    myheadsTransport = myheadsFlow[smallgrid_nodes]
##
##    np.savetxt(os.path.join(curDestDirTrans, "finalheadsFlow.dat"), np.transpose((myheadsTransport)))
##    np.savetxt(os.path.join(curDestDirTrans, curKFile), np.transpose((np.arange(1,len(kkkvalues)+1,1), kkkvalues, kkkvalues, kkkvalues)), fmt='%d %.6e %.6e %.6e')
##    hg.fwdHGS(curDestDirTrans, 'reference', mytype = 'Flow' , hsplot = True)



    #%% Get the "field" observations:
    #----------------------------------------------------
        # Extract modeled heads:
    myouttimesfl = np.loadtxt(os.path.join(mainPath, '..','Model_data','_outputtimestr_.dat'))
    selHeads_All = hg.getmodout(curDestDirTrans, mode = 'tr_', outputtime = myouttimesfl)

        # Extract modeled concentrations:
##    dataPath = curDestDir
##    myouttimestr = np.loadtxt(os.path.join(mainPath, '..','Model_data','_outputtimesTrans.dat'))
##    selConc_All = hg.getmodout(dataPath, mode = 'tr_', outputtime = myouttimestr)

    # Add gaussian measurment noise:
    num_modmeas = selHeads_All.shape[0]
    std_dev = 5.0e-07                                # As an example i am assuming a std error measurement of 1.5 mm on each measurement 1.5e-3, tr = 5.0e-7
    meanNoise = np.zeros((num_modmeas,))            # Mean value of white noise equal to zero (num_modmeas x 1)
    ##R = np.multiply(np.eye(nmeas,nmeas),std_dev**2) # (nmeas x nmeas)
    ##R = np.diag( (np.multiply(np.ones(nmeas,), std_dev**2)) )
    R = ssp.dia_matrix((np.multiply(np.eye(num_modmeas, num_modmeas),std_dev**2))) # sparse Matrix
    E = np.empty((num_modmeas,selHeads_All.shape[1]))
    for ii in range(0,selHeads_All.shape[1],1):
        #E[:,ii] = np.expand_dims(np.random.multivariate_normal(mean,R.toarray()),axis = 1)
        E[:,ii] = np.random.multivariate_normal(meanNoise, R.toarray())

    noisy_data = selHeads_All + E

    # For concentrations
    zero_id = np.where(noisy_data < 0)
    noisy_data[zero_id[0],zero_id[1]] = 0



        # Save Artifical field measurements:
    np.savetxt(os.path.join(mainPath,'Fieldtr_.dat'), noisy_data, fmt = '%10.9e')
##    np.savetxt(os.path.join(mainPath,'Fieldtr_.dat'), selConc_All, fmt = '%10.9e')

    print ('Reference model done and field observations stored')

if __name__ == '__main__':
    main()



